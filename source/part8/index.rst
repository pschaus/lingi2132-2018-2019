.. _part8:

*************************************************************************************************
Partie 8 | Traits and monads
*************************************************************************************************

Questions proposed by Group 36, Guillaume Neirinckx and Julien Bastin
====================================================================

""""""""""""""

Question 1 Explain the differences between traits and abstract classes in Scala :
------------

""""""""""""""

Question 2. Explain why Scala traits are considered more powerful than Java interfaces
------------

""""""""""""""

Question 3. Explain with an example the purpose of trait linearization
------------


""""""""""""""

Answers
=======

Question 1:
----------------------------------------------------------------------

1. Abstract classes don't support multiple inheritances.
2. Traits are stackable, super calls are dynamically bound. An abstract class is not stackable, super calls are statically bound.
3. We are not allowed to add an abstract class to an object instance.
4. Traits do not contain constructor parameters.
5. Abstract class are completely interoperable with Java code, traits as well only if they do not contain any implementation code.
	
	
Question 2:
----------------------------------------------------------------------

Traits, in contrast to java interfaces, are allowed to contain implementation code.

Question 3:
----------------------------------------------------------------------

	.. image:: img/multinher.png
		:height: 200


Scala linearization is a deterministic process that puts all traits in a linear inheritance hierarchy. By doing so we can solve the diamond problem illustrated above. Without it it would be impossible to determine in advance what the return value of the legs function in the following Beast class will be -- i.e 2 or 4.


.. code-block:: scala

    class Beast extends TwoLegged with FourLegged {
        override def legs = super.legs
    }


""""""""""""""



Question proposed by Group 15, Florian Duprez and Yorick Tixhon
=====================================================================

Question 1 What are the differences between Traits and Abstrat class? Show a case where you should favorise Traits.
------------


Question 2 What is a monad? What are the 3 monad laws? Give examples
------------


Question 3 A is an array, and B is a List. If you execute this line, what is the output type?:
------------
.. code-block:: scala

	for( i <- B; <- A ) yield (i,j).

Question 4 Rewrite this for loop using only foreach, map, withFilter and flatMap
------------
.. code-block:: scala

	for (i <- list, j <- array , if (i % 2 == 0)){
		yield i*j
	}

Question 5 Implements l.map(f) with flatMap
------------

Question 6 
------------

Consider the following code:

.. code-block:: scala

	trait A {
		def bob(): String = {"A"}
	}
	trait B {
		def bob(): String = {"B"}
	}
	class Test extends A with B
	val test = new Test
	print(test.bob())
	
Will this code give an error message? If not, what is its output?

Consider now the following variation:

.. code-block:: scala

	trait A {
		def bob(): String = {"A"}
	}
	trait AA extends A {
		override def bob(): String = {"AA"}
	}
	trait AB extends A {
		override def bob(): String = {"AB"}
	}
	class Test extends AB with AA
	val test = new Test
	print(test.bob())
	
Will this new code give an error message? And if not, what is its output?
