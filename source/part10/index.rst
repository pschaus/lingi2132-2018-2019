.. _part10:

*************************************************************************************************
Partie 10 | Traits and monads
*************************************************************************************************

Questions by Group 30, Lionel Ovaert
=====================================================================


1) Traits can be used in a stackable trait pattern. Give an advantage of this pattern and illustrate it with an example.
"""""""""""""""""

2) Illustrate the 3 monad laws with Arrays
"""""""""""""""""

3) What is the result of the following code? Rewrite the for loop using exclusively these operations: foreach, map, withFilter and flatMap.
"""""""""""""""""
.. code-block:: scala

	val mygen = List(List(Some(1)), List(Some(3), None, Some(5)), List(Some(4), Some(5)))
	for (a :: b :: c <- mygen if b != None) yield a


4) You are a young graduate at a job interview and the technical recruiter wants to tests your scala skills.
They use lists that could contain None and would like to do something about it. He asks you to write a function that accepts as input a vector of Option[T].
The method has to return an Option[List[T]] such that if one of the elements is None, the final result is None. Otherwise, the method must return Some(List[T]).
He suggests you to use flatMap, map, list construction (Nil and ::) and pattern matching.
"""""""""""""""""

Monads and Traits
======================================================
**Proposed by Group 7, Madessis Sophie and Van den Bogaert Alexis**

1. Monads
"""""""""""""

* What is a Monad ? 
* What operations is it composed of ?
* Give an example of a Monad.

2. Monad Laws
""""""""""""""""""

What are the 3 laws of Monads? Give an example with two Lists.

3. Secret Option
""""""""""""""""""

3.1 Which usual method of the Option collection is this code corresponding to ?

*You have the following code:*

.. code-block:: java

    option match {
     case None => true
     case Some(x) => foo(x)
    }

3.2 Given the code above corresponds to the method called "secret", what result are the two last lines giving ?

.. code-block:: java

 val a = Some(42)
 val b: Option[Int] = None 
 a.secret(i => Some(2*i)) // Result ?
 b.secret(i => Some(2*i)) // Result ?

3.3 Translate this code using a for/yield loop.




